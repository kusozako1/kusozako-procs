# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

PID = "pid"
NAME = "name"
CMDLINE = "cmdline"
GICON = "gicon"

CPU = "cpu"
MEMORY = "memory"
THREADS = "threads"

CACHE_KEYS = (CPU, MEMORY, THREADS)

PAIRED_SIGNALS = {
    CPU: "cpu_percent_changed",
    MEMORY: "memory-usage-changed",
    THREADS: "n-threads-changed"
}
