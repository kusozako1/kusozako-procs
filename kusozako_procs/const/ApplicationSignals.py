# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

LAYOUT_CHANGED = "layout-changed"               # is_horizontal as bool
TOGGLE_SEARCH_ENTRY = "toggle-search-entry"     # None
SEARCH_CHANGED = "search-changed"               # str
LOCK_TIMEOUT = "lock-timeout"                   # None
UNLOCK_TIMEOUT = "unlock-timeout"               # None
