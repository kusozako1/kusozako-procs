# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_procs.const import ProcessKeys


class DeltaCache(DeltaEntity):

    @classmethod
    def new(cls, parent, signal):
        instance = cls(parent)
        instance.construct(signal)
        return instance

    def refresh(self, value):
        if self._value == value:
            return
        self._value = value
        if value > self._max_value:
            self._max_value = value
        user_data = self._signal, value
        self._raise("delta > cache changed", user_data)

    def construct(self, key):
        self._value = 0
        self._max_value = 0
        self._signal = ProcessKeys.PAIRED_SIGNALS[key]

    def __init__(self, parent):
        self._parent = parent

    @property
    def value(self):
        return self._value

    @property
    def max_value(self):
        return self._max_value
