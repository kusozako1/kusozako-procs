# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_procs.const import ApplicationSignals


class DeltaFinder(Gtk.Revealer, DeltaEntity):

    def _on_search_changed(self, entry):
        user_data = ApplicationSignals.SEARCH_CHANGED, entry.get_text()
        self._raise("delta > application signal", user_data)

    def _toggle(self):
        reveal_child = not self.props.reveal_child
        if reveal_child:
            self._search_bar.grab_focus()
        else:
            self._search_bar.set_text("")
        self.props.reveal_child = reveal_child

    def receive_transmission(self, user_data):
        signal, _ = user_data
        if signal != ApplicationSignals.TOGGLE_SEARCH_ENTRY:
            return
        self._toggle()

    def __init__(self, parent):
        self._parent = parent
        Gtk.Revealer.__init__(self, reveal_child=False)
        self._search_bar = Gtk.SearchEntry(
            placeholder_text=_("Search Process"),
            margin_start=8,
            margin_end=8,
            margin_top=8,
            margin_bottom=8,
            )
        self._search_bar.set_size_request(-1, 30)
        self._search_bar.connect("search-changed", self._on_search_changed)
        self.set_child(self._search_bar)
        self._search_bar.add_css_class("card")
        self._raise("delta > add to container", self)
        self._raise("delta > register application object", self)
