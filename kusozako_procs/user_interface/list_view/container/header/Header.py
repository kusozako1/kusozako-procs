# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .process_box.ProcessBox import DeltaProcessBox
from .StatusBox import DeltaStatusBox


class DeltaHeader(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.append(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.HORIZONTAL,
            homogeneous=True,
            )
        self.set_size_request(-1, 48)
        DeltaProcessBox(self)
        DeltaStatusBox(self)
        self._raise("delta > add to container", self)
