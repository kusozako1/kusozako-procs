# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_procs.const import ProcessSortBy
from .Button import AlfaButton


class DeltaCpu(AlfaButton):

    __label__ = _("CPU")
    __key__ = ProcessSortBy.CPU
