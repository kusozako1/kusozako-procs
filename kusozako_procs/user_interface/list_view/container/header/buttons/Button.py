# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_procs.const import ProcessSortBy

INDICATOR_CSS = "kusozako-button-indicator-highlight"


class AlfaButton(Gtk.Button, DeltaEntity):

    __label__ = "define label here."
    __key__ = "define ownn sort key here."

    def _on_clicked(self, button):
        settings = "process", "sort_by", self.__key__
        self._raise("delta > settings", settings)

    def _reset(self, value):
        if self.__key__ == value:
            self._content.add_css_class(INDICATOR_CSS)
        else:
            self._content.remove_css_class(INDICATOR_CSS)

    def receive_transmission(self, user_data):
        group, key, value = user_data
        if group != "process" or key != "sort_by":
            return
        self._reset(value)

    def __init__(self, parent):
        self._parent = parent
        self._content = Gtk.Label(label=self.__label__)
        self._content.add_css_class("kusozako-text-backlight")
        query = settings = "process", "sort_by", ProcessSortBy.CPU
        settings = self._enquiry("delta > settings", query)
        self._reset(settings)
        self._raise("delta > register settings object", self)
        Gtk.Button.__init__(self, has_frame=False)
        self.add_css_class("kusozako-primary-widget")
        self.set_child(self._content)
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container", self)
