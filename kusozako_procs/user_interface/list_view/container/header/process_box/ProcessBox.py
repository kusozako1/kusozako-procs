# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .SearchButton import DeltaSearchButton


class DeltaProcessBox(Gtk.Box, DeltaEntity):

    def _add_label_center(self, text):
        markup = "<span font_weight='bold'>{}</span>".format(text)
        label = Gtk.Label(hexpand=True)
        label.set_markup(markup)
        label.add_css_class("kusozako-text-backlight")
        self.append(label)

    def _delta_call_add_to_container(self, widget):
        self.append(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.HORIZONTAL,
            homogeneous=False,
            tooltip_text=_("Find Process"),
            )
        DeltaSearchButton(self)
        self._add_label_center(_("Process"))
        self._raise("delta > add to container", self)
