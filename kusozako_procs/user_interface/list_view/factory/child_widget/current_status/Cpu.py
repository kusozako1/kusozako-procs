# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .StatusLabel import AlfaStatusLabel
from kusozako_procs.const import ProcessKeys


class FoxtrotCpu(AlfaStatusLabel):

    SIGNAL = "cpu_percent_changed"

    def _set_label(self, new_value):
        label = "{} %".format(new_value)
        self.set_label(label)

    def _get_value(self):
        return self._proc[ProcessKeys.CPU]
