# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_procs.const import ProcessKeys


class FoxtrotIcon(Gtk.Image):

    def bind(self, proc):
        gicon = proc[ProcessKeys.GICON]
        if gicon is not None:
            self.set_from_gicon(gicon)

    def __init__(self):
        Gtk.Image.__init__(self, pixel_size=32, margin_start=8)
