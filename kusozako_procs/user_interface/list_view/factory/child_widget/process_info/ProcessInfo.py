# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from .Icon import FoxtrotIcon
from .Name import FoxtrotName


class FoxtrotProcessInfo(Gtk.Box):

    def bind(self, proc):
        self._icon.bind(proc)
        self._name.bind(proc)

    def __init__(self):
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        self._icon = FoxtrotIcon()
        self.append(self._icon)
        self._name = FoxtrotName()
        self.append(self._name)
