# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from .base_model.BaseModel import DeltaBaseModel
from .sort_model.SortModel import DeltaSortModel
from .filter_model.FilterModel import DeltaFilterModel
from .SelectionModel import DeltaSelectionModel


class DeltaModels(DeltaEntity):

    def get_selection_model(self):
        return self._selection_model

    def __init__(self, parent):
        self._parent = parent
        base_model = DeltaBaseModel(self)
        sort_model = DeltaSortModel.new(self, base_model)
        filter_model = DeltaFilterModel.new(self, sort_model)
        self._selection_model = DeltaSelectionModel.new(self, filter_model)
