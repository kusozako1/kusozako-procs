# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity


class DeltaRemove(DeltaEntity):

    def _enumerate_procs(self, pids, model):
        for pid in pids:
            for proc in model:
                yield pid, proc

    def _try_remove(self, model, pid, proc=None):
        if proc is None:
            return
        if proc.equal(int(pid)):
            _, position = model.find(proc)
            model.remove(position)

    def remove_pids(self, pids):
        model = self._enquiry("delta > model")
        for pid, proc in self._enumerate_procs(pids, model):
            self._try_remove(model, pid, proc)

    def __init__(self, parent):
        self._parent = parent
