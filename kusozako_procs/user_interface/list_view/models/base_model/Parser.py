# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import psutil
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from kusozako_procs.utility.TimeoutLock import DeltaTimeoutLock


class DeltaParser(DeltaEntity):

    def _parse_pids(self):
        current_pids = set(psutil.pids())
        new_pids = current_pids - self._previous_pids
        deleted_pids = self._previous_pids - current_pids
        if new_pids:
            self._raise("delta > pids added", new_pids)
        if deleted_pids:
            self._raise("delta > pids removed", deleted_pids)
        self._previous_pids = current_pids

    def _timeout(self):
        if not self._lock.is_locked():
            self._parse_pids()
        return GLib.SOURCE_CONTINUE

    def __init__(self, parent):
        self._parent = parent
        self._lock = DeltaTimeoutLock(self)
        self._previous_pids = set()
        GLib.timeout_add_seconds(1, self._timeout)
