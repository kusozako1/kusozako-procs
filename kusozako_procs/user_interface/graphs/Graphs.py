# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_procs.const import ApplicationSignals
from .graph_units.GraphUnits import EchoGraphUnits


class DeltaGraphs(Gtk.FlowBox, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.append(widget)

    def receive_transmission(self, user_data):
        signal, is_horizontal = user_data
        if signal != ApplicationSignals.LAYOUT_CHANGED:
            return
        self._is_horizontal = is_horizontal
        self.invalidate_filter()

    def _filter_func(self, child):
        if self._is_horizontal:
            return True
        return child.get_child().is_essential()

    def __init__(self, parent):
        self._parent = parent
        scrolled_window = Gtk.ScrolledWindow()
        self._is_horizontal = False
        Gtk.FlowBox.__init__(
            self,
            selection_mode=Gtk.SelectionMode.NONE,
            homogeneous=True,
            column_spacing=8,
            margin_start=8,
            margin_end=8,
            )
        self.set_filter_func(self._filter_func)
        EchoGraphUnits(self)
        scrolled_window.set_child(self)
        scrolled_window.add_css_class("kusozako-content-area")
        self._raise("delta > add to container", scrolled_window)
        self._raise("delta > register application object", self)
