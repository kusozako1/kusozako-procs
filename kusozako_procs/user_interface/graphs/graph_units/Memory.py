# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import psutil
from gi.repository import GLib
from kusozako_procs.alfa.bar_graph.BarGraph import AlfaBarGraph

TEMPLATE = "<span font-weight='bold'>{} / {} </span>"


class DeltaMemory(AlfaBarGraph):

    TITLE = "<span font-weight='bold'>Memory</span>"
    BAR_RGBA = (0, 0, 1, 0.5)
    ESSENTIAL = True

    def _delta_info_current_status(self):
        data = psutil.virtual_memory()
        return data.used/data.total*100

    def _delta_info_status_markup(self):
        data = psutil.virtual_memory()
        used = GLib.format_size(data.used)
        total = GLib.format_size(data.total)
        return TEMPLATE.format(used, total)
