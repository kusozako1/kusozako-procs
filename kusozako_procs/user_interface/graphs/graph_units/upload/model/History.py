# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity


class DeltaHistory(DeltaEntity):

    def _push(self, total):
        current = total - self._previous
        self._history.append(current)
        if len(self._history) > 100:
            self._history.pop(0)
        if current > self._max:
            self._max = current
            self._raise("delta > maximum value changed", current)

    def get_history(self):
        return self._history

    def append(self, total):
        if self._previous is not None:
            self._push(total)
        self._previous = total

    def __init__(self, parent):
        self._parent = parent
        self._max = 0
        self._previous = None
        self._history = []
