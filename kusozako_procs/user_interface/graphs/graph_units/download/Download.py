# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .model.Model import DeltaModel
from .StatusLabel import DeltaStatusLabel
from .bar_graph.BarGraph import DeltaBarGraph


class DeltaDownload(Gtk.Box, DeltaEntity):

    ESSENTIAL = False

    def _delta_call_add_to_container(self, widget):
        self.append(widget)

    def _delta_call_model_updated(self):
        self._status_label.refresh()
        self._bar_graph.queue_draw()

    def _delta_info_status_markup(self):
        return self._model.get_status_markup()

    def _delta_info_history(self):
        return self._model.get_history()

    def is_essential(self):
        return self.ESSENTIAL

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        self._model = DeltaModel(self)
        self._status_label = DeltaStatusLabel(self)
        self._bar_graph = DeltaBarGraph(self)
        self._raise("delta > add to container", self)
