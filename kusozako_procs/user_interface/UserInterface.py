# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .observers.LayoutChanged import DeltaLayoutChanged
from .OrientationChanger import DeltaOrientationChanger
from .graphs.Graphs import DeltaGraphs
from .list_view.ListView import DeltaListView


class DeltaUserInterface(Gtk.Paned, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        if self.get_start_child() is None:
            self.set_start_child(widget)
        else:
            self.set_end_child(widget)

    def _delta_info_box(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        Gtk.Paned.__init__(
            self,
            wide_handle=True, orientation=Gtk.Orientation.VERTICAL
            )
        DeltaLayoutChanged(self)
        DeltaOrientationChanger(self)
        DeltaGraphs(self)
        DeltaListView(self)
        self._raise("delta > add to container", self)
