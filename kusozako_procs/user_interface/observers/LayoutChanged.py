# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_procs.const import ApplicationSignals


class DeltaLayoutChanged(DeltaEntity):

    def _change_layout(self, is_horizontal):
        box = self._enquiry("delta > box")
        if is_horizontal:
            box.props.orientation = Gtk.Orientation.HORIZONTAL
            box.set_position(240)
        else:
            box.props.orientation = Gtk.Orientation.VERTICAL
            box.set_position(160)

    def receive_transmission(self, user_data):
        signal, is_horizontal = user_data
        if signal != ApplicationSignals.LAYOUT_CHANGED:
            return
        self._change_layout(is_horizontal)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register application object", self)
