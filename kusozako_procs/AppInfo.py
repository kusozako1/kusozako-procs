# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import re
from gi.repository import Gio

ALL = Gio.AppInfo.get_all()


class FoxtrotAppInfo:

    @classmethod
    def get_default(cls):
        if "_default" not in dir(cls):
            cls._default = cls()
        return cls._default

    def get_executable_name(self, comm, cmdline):
        for element in re.split(" |/", cmdline):
            if element.startswith(comm):
                return element.replace("\n", "")
        return comm.replace("\n", "")

    def get_icon(self, name, cmdline):
        executable_name = self.get_executable_name(name, cmdline)
        for app_info in ALL:
            executable = app_info.get_executable()
            if executable is not None and executable.endswith(executable_name):
                return app_info.get_icon()
        return None
