# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_procs.const import ApplicationSignals


class DeltaTimeoutLock(DeltaEntity):

    def is_locked(self):
        return self._lock

    def receive_transmission(self, user_data):
        signal, _ = user_data
        if signal == ApplicationSignals.LOCK_TIMEOUT:
            self._lock = True
        if signal == ApplicationSignals.UNLOCK_TIMEOUT:
            self._lock = False

    def __init__(self, parent):
        self._parent = parent
        self._lock = False
        self._raise("delta > register application object", self)
