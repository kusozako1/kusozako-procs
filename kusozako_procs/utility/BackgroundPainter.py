# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import math

r = 12


class FoxtrotBackgroundPainter:

    def _clip(self, cairo_context, width, height):
        x = 0
        y = 0
        cairo_context.arc(x+r, y+r, r, math.pi, 3*math.pi/2)
        cairo_context.arc(x+width-r, y+r, r, 3*math.pi/2, 0)
        cairo_context.arc(x+width, y, 0, 0, 0)
        cairo_context.arc(x+width-r, y+height-r, r, 0, math.pi/2)
        cairo_context.arc(x+r, y+height-r, r, math.pi/2, math.pi)
        cairo_context.clip()

    def _grid(self, cairo_context, width, height):
        cairo_context.set_source_rgba(0.5, 0.5, 0.5, 0.1)
        for x in range(width, 0, -10):
            cairo_context.move_to(x, 0)
            cairo_context.line_to(x, height)
            cairo_context.stroke()
        for y in range(0, height, int(height/5)):
            cairo_context.move_to(0, y)
            cairo_context.line_to(width, y)
            cairo_context.stroke()

    def _background(self, cairo_context, width, height):
        cairo_context.set_source_rgba(1, 1, 1, 1)
        cairo_context.rectangle(0, 0, width, height)
        cairo_context.fill()
        self._grid(cairo_context, width, height)

    def paint(self, cairo_context, width, height):
        self._clip(cairo_context, width, height)
        self._background(cairo_context, width, height)
