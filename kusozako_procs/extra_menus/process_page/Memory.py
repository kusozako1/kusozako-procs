# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.SettingsButton import AlfaSettingsButton
from kusozako_procs.const import ProcessSortBy


class DeltaMemory(AlfaSettingsButton):

    LABEL = _("Memory")
    GROUP = "process"
    KEY = "sort_by"
    MATCH_VALUE = ProcessSortBy.MEMORY
