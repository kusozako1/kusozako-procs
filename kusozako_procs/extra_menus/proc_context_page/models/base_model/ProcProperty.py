# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GObject


class FoxtrotProcProperty(GObject.Object):

    def get_value(self):
        return self._value

    def get_name(self):
        return self._name

    def __init__(self, name, value):
        GObject.Object.__init__(self)
        self._name = name
        self._value = value
