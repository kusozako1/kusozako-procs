# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import psutil
from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .ProxySetter import DeltaProxySetter


class DeltaKillButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        self._proc.kill()

    def _delta_call_proxy_changed(self, pid):
        self._proc = psutil.Process(pid=pid)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(
            self,
            label=_("TERMINATE PROCESS"),
            margin_start=8,
            margin_end=8,
            margin_top=8,
            margin_bottom=8,
            opacity=0.8,
            )
        self.add_css_class("destructive-action")
        self.connect("clicked", self._on_clicked)
        DeltaProxySetter(self)
        self._raise("delta > add to container", self)
