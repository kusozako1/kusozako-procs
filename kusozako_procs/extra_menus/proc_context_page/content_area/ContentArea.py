# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.widget.overlay_item.Separator import DeltaSeparator
from .ExitButton import DeltaExitButton
from .properties.Properties import DeltaProperties


class DeltaContentArea(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.append(widget)

    def __init__(self, parent):
        self._parent = parent
        scrolled_window = Gtk.ScrolledWindow(propagate_natural_width=True)
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.VERTICAL,
            hexpand=False,
            vexpand=True,
            margin_top=8,
            margin_bottom=8,
            margin_start=8,
            margin_end=8,
            )
        self.set_size_request(320, -1)
        DeltaExitButton(self)
        DeltaSeparator(self)
        DeltaProperties(self)
        scrolled_window.set_child(self)
        self._raise("delta > add to container", scrolled_window)
