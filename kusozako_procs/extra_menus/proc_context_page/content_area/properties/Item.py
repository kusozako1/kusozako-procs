# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk


class FoxtrotItem(Gtk.Box):

    def __init__(self, proc_property):
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.VERTICAL,
            hexpand=True,
            homogeneous=True,
            )
        self.set_size_request(-1, 32)
        name = proc_property.get_name()
        name_label = Gtk.Label(label=name+" : ", xalign=0)
        self.append(name_label)
        value = proc_property.get_value()
        value_label = Gtk.Label(label=value, xalign=0, wrap=True)
        value_label.add_css_class("dim-label")
        self.append(value_label)
