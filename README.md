# kusozako-procs

system monitor for linux

!["screenshot"](screenshot/screenshot_1.png)
!["screenshot"](screenshot/screenshot_2.png)
!["screenshot"](screenshot/screenshot_3.png)

NOTE: background image is not included. set it yourself.

## License

This software is licensed under GPL v3 or any later version.

see LICENSE.md for more detail.
