# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .clients.Clients import DeltaClients


class AlfaBarGraph(Gtk.Box, DeltaEntity):

    TITLE = "define title markup here."
    BAR_RGBA = "define bar rgba e.g. (1, 0, 0, 0.5)"
    ESSENTIAL = False

    def _delta_info_title_markup(self):
        return self.TITLE

    def _delta_info_bar_rgba(self):
        return self.BAR_RGBA

    def _delta_info_current_status(self):
        raise NotImplementedError

    def _delta_info_status_markup(self):
        raise NotImplementedError

    def _delta_call_current_status_changed(self, current_status):
        self._current_status = current_status

    def _delta_call_add_to_container(self, widget):
        self.append(widget)

    def is_essential(self):
        return self.ESSENTIAL

    def __init__(self, parent):
        self._parent = parent
        self._current_status = None
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        DeltaClients(self)
        self._raise("delta > add to container", self)
