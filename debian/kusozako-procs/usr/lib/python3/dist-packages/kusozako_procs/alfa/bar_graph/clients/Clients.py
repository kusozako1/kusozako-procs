# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from .Model import DeltaModel
from .StatusLabel import DeltaStatusLabel
from .bar_graph.BarGraph import DeltaBarGraph


class DeltaClients(DeltaEntity):

    def _delta_call_timeout(self):
        self._status_label.refresh()
        self._drawing_area.queue_draw()

    def _delta_info_history(self):
        return self._model.get_history()

    def __init__(self, parent):
        self._parent = parent
        self._model = DeltaModel(self)
        self._status_label = DeltaStatusLabel(self)
        self._drawing_area = DeltaBarGraph(self)
