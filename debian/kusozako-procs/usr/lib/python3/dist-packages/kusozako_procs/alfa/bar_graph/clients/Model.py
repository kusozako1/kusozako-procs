# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako1.Entity import DeltaEntity


class DeltaModel(DeltaEntity):

    def _timeout(self):
        current_status = self._enquiry("delta > current status")
        self._history.append(current_status)
        if len(self._history) > 100:
            self._history.pop(0)
        self._raise("delta > current status changed", current_status)
        self._raise("delta > timeout")
        return GLib.SOURCE_CONTINUE

    def get_history(self):
        return self._history

    def __init__(self, parent):
        self._parent = parent
        self._history = []
        GLib.timeout_add_seconds(2, self._timeout)
