# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals
from kusozako_procs.const import ExtraMenuPages


class DeltaProxySetter(DeltaEntity):

    def receive_transmission(self, user_data):
        signal, signal_param = user_data
        if signal != MainWindowSignals.SHOW_EXTRA_PRIMARY_MENU_WITH_PARAM:
            return
        page_name, proc = signal_param
        if page_name == ExtraMenuPages.PROC_CONTEXT_MENU:
            self._raise("delta > proxy changed", proc["pid"])

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register main window signal object", self)
