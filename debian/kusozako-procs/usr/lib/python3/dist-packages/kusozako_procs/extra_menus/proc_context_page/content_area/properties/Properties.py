# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .Item import FoxtrotItem


class DeltaProperties(Gtk.FlowBox, DeltaEntity):

    def _create_widget_func(self, proc_property):
        return FoxtrotItem(proc_property)

    def __init__(self, parent):
        self._parent = parent
        Gtk.FlowBox.__init__(
            self,
            max_children_per_line=1,
            vexpand=True,
            )
        self.bind_model(
            self._enquiry("delta > model"),
            self._create_widget_func
            )
        self._raise("delta > add to container", self)
