# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import psutil
from gi.repository import Gio
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from .ProcProperty import FoxtrotProcProperty
from .ProxySetter import DeltaProxySetter


class DeltaBaseModel(Gio.ListStore, DeltaEntity):

    def _append(self, name, value):
        proc_property = FoxtrotProcProperty(name, value)
        self.append(proc_property)

    def _setup(self, proc):
        self._append("pid", proc.pid)
        self._append("ppid", proc.ppid())
        self._append("name", proc.name())
        self._append("bin", proc.exe())
        self._append("commandline", proc.cmdline())
        # self._append("env", proc.environ())
        date_time = GLib.DateTime.new_from_unix_local(int(proc.create_time()))
        self._append("create time", date_time.format_iso8601())
        self._append("status", proc.status())
        self._append("cwd", proc.cwd())
        self._append("username", proc.username())
        self._append("nice", proc.nice())

    def _delta_call_proxy_changed(self, pid):
        self.remove_all()
        try:
            proc = psutil.Process(pid=pid)
            self._setup(proc)
        except psutil.NoSuchProcess:
            print("process expired", pid)
            return

    def __init__(self, parent):
        self._parent = parent
        Gio.ListStore.__init__(self, item_type=FoxtrotProcProperty)
        DeltaProxySetter(self)
