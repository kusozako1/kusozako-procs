# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.button.Button import AlfaButton
from kusozako1.const import MainWindowSignals


class DeltaProcessButton(AlfaButton):

    START_ICON = "utilities-terminal-symbolic"
    LABEL = _("Process")
    END_ICON = "go-next-symbolic"

    def _on_clicked(self, button):
        user_data = MainWindowSignals.SHOW_EXTRA_PRIMARY_MENU, "process"
        self._raise("delta > main window signal", user_data)

    def _set_to_container(self):
        user_data = MainWindowSignals.ADD_EXTRA_MENU, self
        self._raise("delta > main window signal", user_data)
