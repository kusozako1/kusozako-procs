# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .ProcessButton import DeltaProcessButton
from .proc_context_page.ProcContextPage import DeltaProcContextPage
from .process_page.ProcessPage import DeltaProcessPage


class EchoExtraMenus:

    def __init__(self, parent):
        DeltaProcessButton(parent)
        DeltaProcContextPage(parent)
        DeltaProcessPage(parent)
