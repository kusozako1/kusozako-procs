# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.button.Button import AlfaButton
from kusozako1.const import MainWindowSignals
from kusozako_procs.const import ApplicationSignals


class DeltaFindButton(AlfaButton):

    START_ICON = "edit-find-symbolic"
    LABEL = _("Find")

    def _on_clicked(self, button):
        user_data = ApplicationSignals.TOGGLE_SEARCH_ENTRY, None
        self._raise("delta > application signal", user_data)
        user_data_2 = MainWindowSignals.CLOSE_OVERLAY, None
        self._raise("delta > main window signal", user_data_2)
