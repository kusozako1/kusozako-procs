# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.sub_page.SubPage import AlfaSubPage
from kusozako1.overlay_item.Separator import DeltaSeparator
from kusozako1.overlay_item.Label import DeltaLabel
from .FindButton import DeltaFindButton
from .Cpu import DeltaCpu
from .Memory import DeltaMemory
from .Threads import DeltaThreads


class DeltaProcessPage(AlfaSubPage):

    PAGE_NAME = "process"

    def _on_initialize(self):
        DeltaFindButton(self)
        DeltaSeparator(self)
        DeltaLabel.new_for_label(self, _("Sort by"))
        DeltaCpu(self)
        DeltaMemory(self)
        DeltaThreads(self)
