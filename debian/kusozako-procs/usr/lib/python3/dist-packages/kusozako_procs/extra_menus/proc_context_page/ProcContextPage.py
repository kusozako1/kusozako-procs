# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals
from kusozako1.widget.overlay_item.Spacer import DeltaSpacer
from kusozako_procs.const import ExtraMenuPages
from .models.Models import DeltaModels
from .content_area.ContentArea import DeltaContentArea
from .kill_button.KillButton import DeltaKillButton


class DeltaProcContextPage(Gtk.Box, DeltaEntity):

    def _delta_info_model(self):
        return self._models.get_selection_model()

    def _delta_call_add_to_container(self, widget):
        self.append(widget)

    def __init__(self, parent):
        self._parent = parent
        self._models = DeltaModels(self)
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        DeltaSpacer(self)
        DeltaContentArea(self)
        DeltaKillButton(self)
        self.add_css_class("osd")
        param = self, ExtraMenuPages.PROC_CONTEXT_MENU
        user_data = MainWindowSignals.ADD_EXTRA_PRIMARY_MENU, param
        self._raise("delta > main window signal", user_data)
