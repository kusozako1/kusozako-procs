# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import psutil
from kusozako_procs.AppInfo import FoxtrotAppInfo
from kusozako_procs.const import ProcessKeys

METHODS = {
    ProcessKeys.CPU: "_get_cpu",
    ProcessKeys.MEMORY: "_get_memory",
    ProcessKeys.THREADS: "_get_threads"
}

IMMUTABLES = {
    ProcessKeys.PID: "_pid",
    ProcessKeys.NAME: "_name",
    ProcessKeys.CMDLINE: "_cmdline",
    ProcessKeys.GICON: "_gicon"
}


class FoxtrotProcess:

    @classmethod
    def new(cls, pid):
        instance = cls()
        constructed = instance.construct(pid)
        return instance if constructed else None

    def _get_cpu(self):
        return self._proc.cpu_percent(interval=None)

    def _get_memory(self):
        info = self._proc.memory_info()
        return info.rss - info.shared

    def _get_threads(self):
        return self._proc.num_threads()

    def get_immutable_data(self, key):
        return getattr(self, IMMUTABLES[key])

    def get_status(self, key):
        if not self.exists():
            return 0
        method = getattr(self, METHODS[key])
        return method()

    def exists(self):
        return psutil.pid_exists(self._pid)

    def equal(self, pid):
        return self._pid == pid

    def construct(self, pid):
        try:
            self._proc = psutil.Process(pid=int(pid))
            self._pid = int(pid)
            name = self._proc.name()
            self._cmdline = "  ".join(self._proc.cmdline())
            app_info = FoxtrotAppInfo.get_default()
            self._name = app_info.get_executable_name(name, self._cmdline)
            self._gicon = app_info.get_icon(self._name, self._cmdline)
            return True
        except psutil.NoSuchProcess:
            return False
