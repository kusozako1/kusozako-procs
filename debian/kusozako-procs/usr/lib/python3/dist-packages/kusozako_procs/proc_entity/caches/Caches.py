# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from .Cache import DeltaCache


class DeltaCaches(DeltaEntity):

    def __getitem__(self, key):
        cache = self._caches[key]
        return cache.value

    def refresh(self, process):
        for key, cache in self._caches.items():
            cache.refresh(process.get_status(key))

    def __init__(self, parent):
        self._parent = parent
        self._caches = {}
        self._caches["cpu"] = DeltaCache.new(self, "cpu")
        self._caches["memory"] = DeltaCache.new(self, "memory")
        self._caches["threads"] = DeltaCache.new(self, "threads")
