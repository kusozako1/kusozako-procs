# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GObject
from kusozako1.Entity import DeltaEntity
from kusozako_procs.const import ProcessKeys
from .Process import FoxtrotProcess
from .caches.Caches import DeltaCaches


class DeltaProcEntity(GObject.Object, DeltaEntity):

    __gsignals__ = {
        "cpu_percent_changed": (GObject.SIGNAL_RUN_FIRST, None, (float,)),
        "memory-usage-changed": (GObject.SIGNAL_RUN_FIRST, None, (int,)),
        "n-threads-changed": (GObject.SIGNAL_RUN_FIRST, None, (int,))
    }

    @classmethod
    def new(cls, pid):
        instance = cls()
        constructed = instance.construct(pid)
        return instance if constructed else None

    def __getitem__(self, key):
        if key in ProcessKeys.CACHE_KEYS:
            return self._caches[key]
        return self._process.get_immutable_data(key)

    def _delta_call_cache_changed(self, user_data):
        self.emit(*user_data)

    def refresh(self):
        if self._process.exists():
            self._caches.refresh(self._process)

    def equal(self, pid):
        return self._process.equal(pid)

    def construct(self, pid):
        self._process = FoxtrotProcess.new(pid)
        if self._process is None:
            return False
        self._caches = DeltaCaches(self)
        return True

    def __init__(self):
        self._parent = None
        GObject.Object.__init__(self)
