# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako1.Entity import DeltaEntity

TEMPLATE = "<span font-weight='bold'>{}</span>"


class DeltaStausMarkup(DeltaEntity):

    def _set_markup(self, current_value):
        self._markup = TEMPLATE.format(GLib.format_size(current_value))

    def get_markup(self):
        return self._markup

    def refresh(self, history):
        if not history:
            return
        self._set_markup(history[-1])

    def __init__(self, parent):
        self._parent = parent
        self._markup = "<span font-weight='bold'>please wait</span>"
