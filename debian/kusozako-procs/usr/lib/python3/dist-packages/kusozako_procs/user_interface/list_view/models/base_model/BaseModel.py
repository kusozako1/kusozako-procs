# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from kusozako1.Entity import DeltaEntity
from kusozako_procs.proc_entity.ProcEntity import DeltaProcEntity
from .Append import DeltaAppend
from .Remove import DeltaRemove
from .Parser import DeltaParser


class DeltaBaseModel(Gio.ListStore, DeltaEntity):

    def _delta_call_pids_added(self, pids):
        self._append.append_pids(pids)

    def _delta_call_pids_removed(self, pids):
        self._remove.remove_pids(pids)

    def _delta_info_model(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        Gio.ListStore.__init__(self, item_type=DeltaProcEntity)
        self._append = DeltaAppend(self)
        self._remove = DeltaRemove(self)
        DeltaParser(self)
