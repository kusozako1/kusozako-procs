# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals
from kusozako_procs.const import ExtraMenuPages
from .controllers.Controllers import EchoControllers
from .models.Models import DeltaModels
from .container.Container import DeltaContainer
from .factory.Factory import DeltaFactory


class DeltaListView(Gtk.ListView, DeltaEntity):

    def _delta_info_model(self):
        return self._models.get_selection_model()

    def _delta_call_add_controller(self, controller):
        self.add_controller(controller)

    def _on_activate(self, list_view, index):
        model = list_view.get_model()
        proc_entity = model[index]
        param = ExtraMenuPages.PROC_CONTEXT_MENU, proc_entity
        user_data = MainWindowSignals.SHOW_EXTRA_PRIMARY_MENU_WITH_PARAM, param
        self._raise("delta > main window signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        container = DeltaContainer(self)
        self._models = DeltaModels(self)
        Gtk.ListView.__init__(
            self,
            single_click_activate=True,
            model=self._models.get_selection_model(),
            factory=DeltaFactory(self),
            show_separators=True,
            margin_start=8,
            margin_end=8,
            )
        self.add_css_class("card")
        container.set_list_box(self)
        self.connect("activate", self._on_activate)
        EchoControllers(self)
        self._raise("delta > add to container", container)
