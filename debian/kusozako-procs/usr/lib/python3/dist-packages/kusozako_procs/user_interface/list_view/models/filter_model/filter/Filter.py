# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .SearchQuery import DeltaSearchQuery
from .SortKey import DeltaSortKey


class DeltaFilter(Gtk.CustomFilter, DeltaEntity):

    def _filter_func(self, proc, user_data=None):
        if self._search_query.has_query():
            return self._search_query.matches(proc)
        return self._sort_key.matches(proc)

    def _delta_call_filter_changed(self):
        self.changed(Gtk.FilterChange.DIFFERENT)

    def __init__(self, parent):
        self._parent = parent
        self._search_query = DeltaSearchQuery(self)
        self._sort_key = DeltaSortKey(self)
        Gtk.CustomFilter.__init__(self)
        self.set_filter_func(self._filter_func)
