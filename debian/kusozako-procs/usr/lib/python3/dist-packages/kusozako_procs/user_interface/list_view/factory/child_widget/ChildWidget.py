# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from .process_info.ProcessInfo import FoxtrotProcessInfo
from .current_status.CurrentStatus import FoxtrotCurrentStatus


class FoxtrotChildWidget(Gtk.Box):

    def bind(self, proc):
        self._process_info.bind(proc)
        self._current_status.bind(proc)

    def unbind(self):
        self._current_status.unbind()

    def __init__(self):
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.HORIZONTAL,
            homogeneous=True,
            )
        self.set_size_request(-1, 32)
        self._process_info = FoxtrotProcessInfo()
        self.append(self._process_info)
        self._current_status = FoxtrotCurrentStatus()
        self.append(self._current_status)
