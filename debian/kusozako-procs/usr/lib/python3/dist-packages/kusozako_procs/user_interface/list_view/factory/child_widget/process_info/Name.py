# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Pango
from kusozako_procs.const import ProcessKeys


class FoxtrotName(Gtk.Box):

    def bind(self, proc):
        self._name.set_label(proc[ProcessKeys.NAME])
        self._cmdline.set_label(proc[ProcessKeys.CMDLINE])

    def __init__(self):
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.VERTICAL,
            homogeneous=True,
            margin_start=8,
            )
        self.set_size_request(-1, 48)
        self._name = Gtk.Label(
            xalign=0,
            yalign=0.8,
            ellipsize=Pango.EllipsizeMode.MIDDLE
            )
        self.append(self._name)
        self._cmdline = Gtk.Label(
            xalign=0,
            yalign=0.2,
            ellipsize=Pango.EllipsizeMode.MIDDLE
            )
        self._cmdline.add_css_class("dim-label")
        self.append(self._cmdline)
