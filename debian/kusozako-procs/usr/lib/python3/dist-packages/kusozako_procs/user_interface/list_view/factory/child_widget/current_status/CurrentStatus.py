# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from .Cpu import FoxtrotCpu
from .Memory import FoxtrotMemory
from .Threads import FoxtrotThreads


class FoxtrotCurrentStatus(Gtk.Box):

    def bind(self, proc):
        self._cpu.bind(proc)
        self._memory.bind(proc)
        self._threads.bind(proc)

    def unbind(self):
        self._cpu.unbind()
        self._memory.unbind()
        self._threads.unbind()

    def __init__(self):
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.HORIZONTAL,
            homogeneous=True,
            )
        self.set_size_request(-1, 32)
        self._cpu = FoxtrotCpu()
        self.append(self._cpu)
        self._memory = FoxtrotMemory()
        self.append(self._memory)
        self._threads = FoxtrotThreads()
        self.append(self._threads)
