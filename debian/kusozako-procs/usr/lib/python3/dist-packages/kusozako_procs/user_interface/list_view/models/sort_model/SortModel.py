# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from kusozako_procs.utility.TimeoutLock import DeltaTimeoutLock
from .Sorter import DeltaSorter


class DeltaSortModel(Gtk.SortListModel, DeltaEntity):

    @classmethod
    def new(cls, parent, parent_model):
        instance = cls(parent)
        instance.construct(parent_model)
        return instance

    def _refresh(self, sorter):
        for proc in self:
            proc.refresh()
        sorter.changed(Gtk.SorterChange.DIFFERENT)

    def _timeout(self, sorter):
        if not self._lock.is_locked():
            self._refresh(sorter)
        return GLib.SOURCE_CONTINUE

    def construct(self, parent_model):
        self.set_model(parent_model)

    def __init__(self, parent):
        self._parent = parent
        self._lock = DeltaTimeoutLock(self)
        sorter = DeltaSorter(self)
        Gtk.SortListModel.__init__(self, sorter=sorter)
        GLib.timeout_add_seconds(2, self._timeout, sorter)
