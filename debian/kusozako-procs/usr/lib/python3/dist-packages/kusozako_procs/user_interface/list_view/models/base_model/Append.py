# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_procs.const import ProcessKeys
from kusozako_procs.proc_entity.ProcEntity import DeltaProcEntity


class DeltaAppend(DeltaEntity):

    def _try_append(self, pid):
        proc = DeltaProcEntity.new(str(pid))
        if proc is None:
            return
        name = proc[ProcessKeys.NAME]
        if name.startswith("kworker"):
            return
        model = self._enquiry("delta > model")
        model.append(proc)

    def append_pids(self, pids):
        for pid in pids:
            self._try_append(pid)

    def __init__(self, parent):
        self._parent = parent
