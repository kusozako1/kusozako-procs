# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Cpu import DeltaCpu
from .Memory import DeltaMemory
from .Threads import DeltaThreads


class EchoButtons:

    def __init__(self, parent):
        DeltaCpu(parent)
        DeltaMemory(parent)
        DeltaThreads(parent)
