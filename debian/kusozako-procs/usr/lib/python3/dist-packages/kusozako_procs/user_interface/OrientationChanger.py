# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_procs.const import ApplicationSignals


class DeltaOrientationChanger(DeltaEntity):

    def _on_layout(self, surface, width, height, widget):
        is_horizontal = True if width > 800 else False
        if is_horizontal == self._is_horizontal:
            return
        self._is_horizontal = is_horizontal
        user_data = ApplicationSignals.LAYOUT_CHANGED, is_horizontal
        self._raise("delta > application signal", user_data)

    def _on_realize(self, widget):
        native = widget.get_native()
        surface = native.get_surface()
        surface.connect("layout", self._on_layout, widget)

    def __init__(self, parent):
        self._parent = parent
        self._is_horizontal = False
        box = self._enquiry("delta > box")
        box.connect("realize", self._on_realize)
