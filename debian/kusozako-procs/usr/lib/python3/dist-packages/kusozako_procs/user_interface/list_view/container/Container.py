# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .header.Header import DeltaHeader
from .Finder import DeltaFinder


class DeltaContainer(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.append(widget)

    def set_list_box(self, list_box):
        self._inner_box.append(list_box)
        self._inner_box.append(Gtk.Box(vexpand=True))

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        DeltaHeader(self)
        DeltaFinder(self)
        scrolled_window = Gtk.ScrolledWindow()
        self.append(scrolled_window)
        self._inner_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        scrolled_window.set_child(self._inner_box)
        self.add_css_class("kusozako-content-area")
