# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_procs.const import ProcessKeys


class DeltaSorter(Gtk.CustomSorter, DeltaEntity):

    def _sort_func(self, alfa, bravo, user_data=None):
        alfa_value = alfa[self._current_key]
        bravo_value = bravo[self._current_key]
        result = alfa_value - bravo_value
        if result == 0:
            return 0
        return -1 if result > 0 else 1

    def receive_transmission(self, user_data):
        group, key, value = user_data
        if group != "process" or key != "sort_by":
            return
        self._current_key = value
        self.changed(Gtk.SorterChange.DIFFERENT)

    def __init__(self, parent):
        self._parent = parent
        query = "process", "sort_by", ProcessKeys.CPU
        self._current_key = self._enquiry("delta > settings", query)
        Gtk.CustomSorter.__init__(self)
        self.set_sort_func(self._sort_func)
        self._raise("delta > register settings object", self)
