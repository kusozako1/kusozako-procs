# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Motion import DeltaMotion


class EchoControllers:

    def __init__(self, parent):
        DeltaMotion(parent)
