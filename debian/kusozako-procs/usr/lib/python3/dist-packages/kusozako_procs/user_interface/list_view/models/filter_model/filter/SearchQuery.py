# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from kusozako_procs.const import ApplicationSignals
from kusozako_procs.const import ProcessKeys


class DeltaSearchQuery(DeltaEntity):

    def matches(self, proc):
        if self._query in GLib.utf8_strdown(proc[ProcessKeys.NAME], -1):
            return True
        return self._query in GLib.utf8_strdown(proc[ProcessKeys.CMDLINE], -1)

    def has_query(self):
        return self._query != ""

    def receive_transmission(self, user_data):
        signal, query = user_data
        if signal != ApplicationSignals.SEARCH_CHANGED:
            return
        self._query = GLib.utf8_strdown(query, -1)
        self._raise("delta > filter changed")

    def __init__(self, parent):
        self._parent = parent
        self._query = ""
        self._raise("delta > register application object", self)
