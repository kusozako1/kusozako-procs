# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_procs.const import ApplicationSignals


class DeltaMotion(Gtk.EventControllerMotion, DeltaEntity):

    def _on_enter(self, *args):
        user_data = ApplicationSignals.LOCK_TIMEOUT, None
        self._raise("delta > application signal", user_data)

    def _on_leave(self, *args):
        user_data = ApplicationSignals.UNLOCK_TIMEOUT, None
        self._raise("delta > application signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.EventControllerMotion.__init__(self)
        self.connect("leave", self._on_leave)
        self.connect("enter", self._on_enter)
        self._raise("delta > add controller", self)
