# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from .StatusLabel import AlfaStatusLabel
from kusozako_procs.const import ProcessKeys


class FoxtrotMemory(AlfaStatusLabel):

    SIGNAL = "memory-usage-changed"

    def _set_label(self, memory_usage):
        usage = GLib.format_size_full(
            memory_usage,
            # GLib.FormatSizeFlags.IEC_UNITS
            GLib.FormatSizeFlags.DEFAULT
            )
        self.set_label(usage)

    def _get_value(self):
        return self._proc[ProcessKeys.MEMORY]
