# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .StatusLabel import AlfaStatusLabel
from kusozako_procs.const import ProcessKeys


class FoxtrotThreads(AlfaStatusLabel):

    SIGNAL = "n-threads-changed"

    def _set_label(self, value):
        self.set_label(str(value))

    def _get_value(self):
        return self._proc[ProcessKeys.THREADS]
