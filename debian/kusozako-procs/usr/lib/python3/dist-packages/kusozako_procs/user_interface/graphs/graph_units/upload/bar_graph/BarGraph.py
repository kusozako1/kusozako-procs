# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_procs.utility.BackgroundPainter import FoxtrotBackgroundPainter
from .BarPainter import DeltaBarPainter


class DeltaBarGraph(Gtk.DrawingArea, DeltaEntity):

    def _draw_func(self, drawing_area, cairo_context, width, height):
        self._background_painter.paint(cairo_context, width, height)
        self._bar_painter.paint(cairo_context, width, height)

    def __init__(self, parent):
        self._parent = parent
        Gtk.DrawingArea.__init__(self)
        self._background_painter = FoxtrotBackgroundPainter()
        self._bar_painter = DeltaBarPainter(self)
        self.set_size_request(-1, 100)
        self.set_draw_func(self._draw_func)
        self._raise("delta > add to container", self)
