# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_procs.const import ProcessKeys


class DeltaSortKey(DeltaEntity):

    def _is_valid(self, proc):
        if proc[ProcessKeys.CPU] == 0:
            return False
        return proc[ProcessKeys.MEMORY] > 0

    def matches(self, proc):
        if proc[self._current_key] > 0:
            return True
        return self._is_valid(proc)

    def receive_transmission(self, user_data):
        group, key, value = user_data
        if group != "process" or key != "sort_by":
            return
        self._current_key = value
        self._raise("delta > filter changed")

    def __init__(self, parent):
        self._parent = parent
        query = "process", "sort_by", ProcessKeys.CPU
        self._current_key = self._enquiry("delta > settings", query)
        self._raise("delta > register settings object", self)
