# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Cpu import DeltaCpu
from .Memory import DeltaMemory
from .Swap import DeltaSwap
from .logical_cores.LogicalCores import DeltaLogicalCores
from .upload.Upload import DeltaUpload
from .download.Download import DeltaDownload


class EchoGraphUnits:

    def __init__(self, parent):
        DeltaCpu(parent)
        DeltaMemory(parent)
        DeltaSwap(parent)
        DeltaLogicalCores(parent)
        DeltaUpload(parent)
        DeltaDownload(parent)
