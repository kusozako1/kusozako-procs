# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals


class DeltaHorizontalPositionChanged(DeltaEntity):

    def _on_notify(self, paned, gparam):
        settings = "view", "horizontal_position", paned.props.position
        self._raise("delta > settings", settings)

    def receive_transmission(self, user_data):
        signal, _ = user_data
        if signal != MainWindowSignals.GREETER_CLOSED:
            return
        paned = self._enquiry("delta > box")
        paned.connect("notify", self._on_notify)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register main window signal object", self)
