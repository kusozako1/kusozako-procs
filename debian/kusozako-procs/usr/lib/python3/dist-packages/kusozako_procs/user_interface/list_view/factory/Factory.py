# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .child_widget.ChildWidget import FoxtrotChildWidget


class DeltaFactory(Gtk.SignalListItemFactory, DeltaEntity):

    def _on_setup(self, factory, list_item):
        child_widget = FoxtrotChildWidget()
        list_item.set_child(child_widget)

    def _on_bind(self, factory, list_item):
        proc = list_item.get_item()
        child_widget = list_item.get_child()
        child_widget.bind(proc)

    def _on_unbind(self, factory, list_item):
        child_widget = list_item.get_child()
        child_widget.unbind()

    def __init__(self, parent):
        self._parent = parent
        Gtk.SignalListItemFactory.__init__(self)
        self.connect("setup", self._on_setup)
        self.connect("bind", self._on_bind)
        self.connect("unbind", self._on_unbind)
