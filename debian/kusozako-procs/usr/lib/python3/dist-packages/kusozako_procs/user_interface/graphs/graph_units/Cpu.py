# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import psutil
from kusozako_procs.alfa.bar_graph.BarGraph import AlfaBarGraph

TEMPLATE = "<span font-weight='bold'>{} % / {} GHz</span>"


class DeltaCpu(AlfaBarGraph):

    TITLE = "<span font-weight='bold'>CPU</span>"
    BAR_RGBA = (1, 0, 0, 0.5)
    ESSENTIAL = True

    def _delta_info_current_status(self):
        return psutil.cpu_percent()

    def _delta_info_status_markup(self):
        cpu_freq = round(psutil.cpu_freq().current/1000, 2)
        return TEMPLATE.format(self._current_status, cpu_freq)
