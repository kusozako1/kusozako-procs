# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity

TEMPLATE = "<span font-weight='bold'>{} %</span>"


class DeltaStatusLabel(Gtk.Box, DeltaEntity):

    def refresh(self, current_value):
        markup = TEMPLATE.format(current_value)
        self._value.set_markup(markup)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.HORIZONTAL,
            )
        title = Gtk.Label(xalign=0, margin_start=16)
        title.set_markup(self._enquiry("delta > title markup"))
        title.set_size_request(-1, 48)
        title.add_css_class("kusozako-text-backlight")
        self.append(title)
        self._value = Gtk.Label(
            label="please wait",
            xalign=1,
            margin_end=16,
            hexpand=True
            )
        self._value.add_css_class("kusozako-text-backlight")
        self.append(self._value)
        self._raise("delta > add to container", self)
