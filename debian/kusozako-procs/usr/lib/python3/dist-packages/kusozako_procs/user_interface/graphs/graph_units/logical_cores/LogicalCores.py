# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import psutil
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from .graph_unit.GraphUnit import DeltaGraphUnit


class DeltaLogicalCores(DeltaEntity):

    def _timeout(self):
        data = psutil.cpu_percent(interval=None, percpu=True)
        for graph_unit in self._graph_units:
            graph_unit.refresh(data.pop(0))
        return GLib.SOURCE_CONTINUE

    def _setup(self):
        for index in range(0, psutil.cpu_count(logical=True)):
            graph_unit = DeltaGraphUnit.new(self, index)
            self._graph_units.append(graph_unit)
        GLib.timeout_add_seconds(2, self._timeout)

    def __init__(self, parent):
        self._parent = parent
        self._graph_units = []
        self._setup()
