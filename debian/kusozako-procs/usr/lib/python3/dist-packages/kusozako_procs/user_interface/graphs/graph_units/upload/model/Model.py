# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import psutil
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from .History import DeltaHistory
from .StatusMarkup import DeltaStausMarkup


class DeltaModel(DeltaEntity):

    def _timeout(self):
        data = psutil.net_io_counters()
        self._history.append(data.bytes_sent)
        self._raise("delta > model updated")
        current_value = self._history.get_history()
        self._status_markup.refresh(current_value)
        return GLib.SOURCE_CONTINUE

    def _delta_call_maximum_value_changed(self, maximum_value):
        if maximum_value > self._max:
            self._max = maximum_value

    def get_history(self):
        return self._max, self._history.get_history()

    def get_status_markup(self):
        return self._status_markup.get_markup()

    def __init__(self, parent):
        self._parent = parent
        self._max = 1
        self._history = DeltaHistory(self)
        self._status_markup = DeltaStausMarkup(self)
        GLib.timeout_add_seconds(2, self._timeout)
