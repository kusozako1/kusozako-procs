# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .StatusLabel import DeltaStatusLabel
from .bar_graph.BarGraph import DeltaBarGraph


class DeltaGraphUnit(Gtk.Box, DeltaEntity):

    ESSENTIAL = False

    @classmethod
    def new(cls, parent, index):
        instance = cls(parent)
        instance.construct(index)
        return instance

    def construct(self, index):
        self._title = "<span font-weight='bold'>Core {}</span>".format(index)
        self._status_label = DeltaStatusLabel(self)
        self._bar_graph = DeltaBarGraph(self)

    def refresh(self, current_status):
        self._status_label.refresh(current_status)
        self._bar_graph.refresh(current_status)

    def _delta_info_title_markup(self):
        return self._title

    def _delta_info_bar_rgba(self):
        return (1, 0, 1, 0.5)

    def _delta_call_add_to_container(self, widget):
        self.append(widget)

    def is_essential(self):
        return self.ESSENTIAL

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        self._raise("delta > add to container", self)
