# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .filter.Filter import DeltaFilter


class DeltaFilterModel(Gtk.FilterListModel, DeltaEntity):

    @classmethod
    def new(cls, parent, parent_model):
        instance = cls(parent)
        instance.construct(parent_model)
        return instance

    def construct(self, parent_model):
        self.set_model(parent_model)

    def __init__(self, parent):
        self._parent = parent
        Gtk.FilterListModel.__init__(self, filter=DeltaFilter(self))
