# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk


class AlfaStatusLabel(Gtk.Label):

    SIGNAL = "define signal to connect here."

    def _set_label(self, new_value):
        raise NotImplementedError

    def _get_value(self):
        raise NotImplementedError

    def _on_signal(self, proc, new_value):
        self._set_label(new_value)

    def bind(self, proc):
        self._proc = proc
        self._id = self._proc.connect(self.SIGNAL, self._on_signal)
        self._set_label(self._get_value())

    def unbind(self):
        self._proc.disconnect(self._id)

    def __init__(self):
        Gtk.Label.__init__(self, xalign=1, margin_end=8)
