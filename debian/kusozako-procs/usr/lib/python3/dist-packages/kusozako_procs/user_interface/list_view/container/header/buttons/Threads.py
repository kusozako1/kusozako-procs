# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_procs.const import ProcessSortBy
from .Button import AlfaButton


class DeltaThreads(AlfaButton):

    __label__ = _("Threads")
    __key__ = ProcessSortBy.THREADS
