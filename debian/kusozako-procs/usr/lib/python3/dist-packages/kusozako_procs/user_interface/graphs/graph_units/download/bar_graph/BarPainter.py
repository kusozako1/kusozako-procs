# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity

STEP = 10


class DeltaBarPainter(DeltaEntity):

    def paint(self, cairo_context, width, height):
        cairo_context.set_source_rgba(*self._rgba)
        maximum_value, raw_history = self._enquiry("delta > history")
        history = raw_history.copy()
        x_position = width - len(history)*STEP
        cairo_context.move_to(x_position, 0)
        unit = height/maximum_value
        while history or x_position > width:
            usage = history.pop(0)
            y = height - usage*unit
            cairo_context.rectangle(x_position, y, STEP, height-y)
            cairo_context.fill()
            x_position += STEP

    def __init__(self, parent):
        self._parent = parent
        self._rgba = (1, 1, 1, 0.5)
